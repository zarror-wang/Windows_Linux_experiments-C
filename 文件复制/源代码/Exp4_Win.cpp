#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <Windowsx.h>
#include <string.h>

void CopyFile(char *fsource, char *ftarget) //复制文件的主进程段
{
    WIN32_FIND_DATA lpfindfiledata; //获取文件描述符结构体
    printf("Creating the File!\n");
    HANDLE hfind = FindFirstFile(fsource, &lpfindfiledata); //获取源文件文件描述符
    HANDLE hsource = CreateFile(fsource, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    HANDLE htarget = CreateFile(ftarget, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    LONG size = lpfindfiledata.nFileSizeLow - lpfindfiledata.nFileSizeHigh; //获取源文件大小
    DWORD wordbit;
    int *BUFFER = new int[size];                      //定义缓冲区
    ReadFile(hsource, BUFFER, size, &wordbit, NULL);  //从源文件读
    WriteFile(htarget, BUFFER, size, &wordbit, NULL); //向目标文件写
    CloseHandle(hfind);
    CloseHandle(hsource);
    CloseHandle(htarget);
}

void mycopy(char *fsource, char *ftarget)
{
    char source[500];
    char target[500];
    WIN32_FIND_DATA lpfindfiledata;
    strcpy(source, fsource);
    strcpy(target, ftarget);
    strcat(source, "\\*.*");
    strcat(target, "\\");
    HANDLE hfind = FindFirstFile(source, &lpfindfiledata);
    if (hfind != INVALID_HANDLE_VALUE)
    {
        while (FindNextFile(hfind, &lpfindfiledata) != 0)//当源文件夹部位空时
        {
            if ((lpfindfiledata.dwFileAttributes) & 16) //当目标是文件夹
            {
                if ((strcmp(lpfindfiledata.cFileName, ".") != 0) && (strcmp(lpfindfiledata.cFileName, "..") != 0))//访问文件夹下的文件
                {
                    memset(source, '0', sizeof(source));
                    strcpy(source, fsource);
                    strcat(source, "\\");
                    strcat(source, lpfindfiledata.cFileName);
                    strcat(target, lpfindfiledata.cFileName);
                    CreateDirectoryA(target, NULL);
                    printf("Creating the Directory!\n");
                    mycopy(source, target);
                    strcpy(source, fsource);
                    strcat(source, "\\");
                    strcpy(target, ftarget);
                    strcat(target, "\\");
                }
            }
            else //文件
            {
                memset(source, '0', sizeof(source));
                strcpy(source, fsource);
                strcat(source, "\\");
                strcat(source, lpfindfiledata.cFileName);
                strcat(target, lpfindfiledata.cFileName);
                CopyFile(source, target);
                strcpy(source, fsource);
                strcat(source, "\\");
                strcpy(target, ftarget);
                strcat(target, "\\");
            }
        }
    }
}

int main(int argc, char *argv[])
{
    WIN32_FIND_DATA lpfindfiledata;

    if (argc != 3)
    {
        printf("Error Parameters\n");
    }
    else
    {
        if (FindFirstFile(argv[1], &lpfindfiledata) == INVALID_HANDLE_VALUE)
        {
            printf("Cannot Find the Source File\n");
        }
        if (FindFirstFile(argv[2], &lpfindfiledata) == INVALID_HANDLE_VALUE) //没有目标文件夹则创建
        {
            CreateDirectory(argv[2], NULL);
        }
        mycopy(argv[1], argv[2]);
    }
    printf("Copy Finish\n");
    return 0;
}