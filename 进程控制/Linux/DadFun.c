#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include<sys/time.h>

int main(int argc,char* argv[])
{
    struct timeval tv_begin,tv_end;
    pid_t fpid;
    fpid = fork();
    if (fpid < 0)
        printf("创建进程失败!");
    else if (fpid == 0)
    {
       
        printf("这里是子进程段!\n");
        char * str[] = {argv[1], "Andyoyo",NULL};
        char pstr[]={"/home/zarror/桌面/Exp1/"};
        strcat(pstr,argv[1]);
        execv(pstr,str);
       
    }
    else
    { 
        gettimeofday(&tv_begin,NULL); 
        wait(NULL); 
        gettimeofday(&tv_end,NULL); 
        
        printf("子进程时间%ldus\n",(tv_end.tv_usec-tv_begin.tv_usec));
    }
   
   
    return 0;
}
