#include <stdio.h>
#include <windows.h>
int main(int argc, char *argv[])
{
    STARTUPINFO si = {sizeof(si)};
    SYSTEMTIME utcSysTime1, utcSysTime2;
    PROCESS_INFORMATION p;
    si.dwFlags = STARTF_USESHOWWINDOW; //指定wShowWindow成员效
    si.wShowWindow = TRUE;             //此成员设为TRUE的话则显示新建进程的主窗口
    GetSystemTime(&utcSysTime1);
    BOOL bRet = CreateProcess(
        NULL,               //不在此指定可执行文件的文件名
        argv[1],            //命令行参数
        NULL,               //默认进程安全性
        NULL,               //默认进程安全性
        FALSE,              //指定当前进程内句柄不可以被子进程继承
        CREATE_NEW_CONSOLE, //为新进程创建一个新的控制台窗口
        NULL,               //使用本进程的环境变量
        NULL,               //使用本进程的驱动器和目录
        &si,
        &p);
     WaitForSingleObject(p.hProcess, INFINITE);
     GetSystemTime(&utcSysTime2);
     printf("%u ms\n",utcSysTime2.wMilliseconds-utcSysTime1.wMilliseconds);
     if(bRet)
    {
        CloseHandle(p.hProcess);
        printf("%d\n",p.dwProcessId);
    }
    

    system("pause");
    return 0;
}