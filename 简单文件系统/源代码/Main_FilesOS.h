#ifndef Main_FileOS_H
#define Main_FileOS_H

//引入关键库函数
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//对关键字大小进行定义
#define BlockSize 1024  //定义数据块的大小
#define OS_Size 1024000 //定义内存区域文件系统大小
#define END 65535
#define Free 0
#define Boot_Block 2        //定义文件系统启动区位置
#define Max_File_Open 10    //定义最大打开文件数
#define Max_Text_Size 10000 //定义文件最大大小
#define CD_Temp_Size 10000

//对文件系统关键结构体进行定义
typedef struct FCB //定义文件控制块
{
    char File_Name[10];         //文件名
    char EX_Name[3];            //文件扩展名
    char Is_Free;               //文件控制块状态 0代表空文件控制块
    unsigned char Attribute;    //文件属性  0代表目录文件 1代表标准文件
    unsigned short time;        //文件建立的时间信息
    unsigned short date;        //文件建立的日期信息
    unsigned short First_Block; //文件存放的首个数据块
    unsigned short Length;      //文件大小
} fcb;

typedef struct FAT //文件分配表
{
    unsigned short ID;
} fat;

typedef struct User_Open //用户打开文件表
{
    char File_Name[10];          //文件名
    char EX_Name[3];             //文件扩展名
    char Is_Free;                //文件控制块状态 0代表空文件控制块
    unsigned char Attribute;     //文件属性  0代表目录文件 1代表标准文件
    unsigned short time;         //文件建立的时间信息
    unsigned short date;         //文件建立的日期信息
    unsigned short First_Block;  //文件存放的首个数据块
    unsigned short Length;       //文件大小
    int Dir_Num;                 //父目录起始盘块号
    int Diroff;                  //该文件对应的FCB在父目录中的逻辑序号
    char Dir[Max_File_Open][80]; //全路径信息
    int Cont;                    //链接计数
    char FcbState;               //是否修改 1修改 0未修改
    char TopenFile;              //0: 空
} user_open;

typedef struct Block
{
    char Magic_Num[8]; //魔数
    char Info[200];    //相关信息
    unsigned short root;
    unsigned char *Start_Block;
} block0;

unsigned char *myvhard;
user_open opfilelist[Max_File_Open];
int currfd;
unsigned char *startp;
unsigned char buffer[OS_Size];

//文件系统函数定义
int Start_File_System(int model);                             //ok
void Format(char *filename);                                  //ok
void CD(char *dirname);                                       //ok
int Sub_Read(int fd, int len, char *text);                    //ok
int Read(int fd);                                             //ok
int Write(int fd);                                            //ok
int Open(char *filename);                                     //ok
int Close(int fd);                                            //ok
void Mkdir(char *dirname);                                    //ok
void Rmdir(char *dirname);                                    //ok
int Create(char *filename);                                   //ok
void Remove(char *fiilename);                                 //ok
void List();                                                  //ok
void Exit_File_System();                                      //ok
int Sub_Write(int fd, char *text, int len, char write_style); //ok
int Get_Free_openfilelist();                                  //ok
void help();
unsigned short int Get_Free_Block(); //ok
#endif