#include "Main_FileOS.c"

int main()
{
    char cmd[15][11] = {"mkdir", "rmdir", "ls", "cd", "create", "open", "close", "read", "write", "delete", "exit", "new", "sys", "help"};
    char *p;
    char fileosname[15];
    char cmd_line[20];
    int cmd_index = 0;
    int model;
    printf("--------------------------WELCOME TO FILE_SYSTEM---------------------------\n");
    help();
    printf("--------------------Please OPEN or Create a File_System-------------------\n");
    while (1)
    {
        gets(cmd_line);
        p = strtok(cmd_line, " ");
        if (strcmp(p, cmd[11]) == 0) //创建一个新的文件系统
        {
            p = strtok(NULL, " ");
            Save_File_Name = p;
            int i = 0;
            while (*p != '\0')
            {
                fileosname[i] = *p;
                i++;
                p++;
            }
            // fileosname[i] = '\0';
            if (Start_File_System(1) != 0)
                continue;
            else
                break;
        }
        else if (strcmp(p, cmd[12]) == 0)
        {
            p = strtok(NULL, " ");
            Save_File_Name = p;
            int i = 0;
            while (*p != '\0')
            {
                fileosname[i] = *p;
                i++;
                p++;
            }
            //   fileosname[i] = '\0';
            if (Start_File_System(2) != 0)
                continue;
            else
                break;
        }
    }

    while (1)
    {
        printf("%s> ", opfilelist[currfd].Dir);
        gets(cmd_line);
        cmd_index = -1;
        if (strcmp(cmd_line, "") == 0)
        {
            printf("\n");
            continue;
        }

        p = strtok(cmd_line, " ");
        for (int i = 0; i < 14; i++)
            if (strcmp(p, cmd[i]) == 0)
            {
                cmd_index = i;
                break;
            }

        switch (cmd_index)
        {
        case 0: //mkdir
            p = strtok(NULL, " ");
            if (p != NULL)
                Mkdir(p);
            else
                printf("Failed to make the Folder\n");
            break;
        case 1: //rmdir
            p = strtok(NULL, " ");
            if (p != NULL)
                Rmdir(p);
            else
                printf("Failed to remove the Folder!\n");
            break;
        case 2: //ls
            List();
            break;
        case 3: //cd
            p = strtok(NULL, " ");
            if (p != NULL)
                CD(p);
            else
                printf("Failed to open the Folder!\n");
            break;
        case 4: //create
            p = strtok(NULL, " ");
            if (p != NULL)
                Create(p);
            else
                printf("Failed to Create the File!\n");
            break;
        case 5: //open
            p = strtok(NULL, " ");
            if (p != NULL)
                Open(p);
            else
                printf("Failed to open the File!\n");
            break;
        case 6: //close
            if (opfilelist[currfd].Attribute == 1)
                Close(currfd);
            else
                printf("You haven't open any file yet!\n");
            break;
        case 7: //read
            if (opfilelist[currfd].Attribute == 1)
                Read(currfd);
            else
                printf("Please open the File first,then Read!\n");
            break;
        case 8: //write
            if (opfilelist[currfd].Attribute == 1)
                Write(currfd);
            else
                printf("Please open the File first,then Write!\n");
            break;
        case 9: //delete
            p = strtok(NULL, " ");
            if (p != NULL)
                Remove(p);
            else
                printf("Delete Standrad file Failed!\n");
            break;
        case 10: //exit
            Save_File_Name = &fileosname;
            Exit_File_System();
            printf("----------------------Exit the File_System Sucessfully---------------------\n");
            return 0;
            break;
        case 13: //help
            help();
            break;

        default:
            printf("ERROR Command,Please try again!\n");
            break;
        }
    }
    return 0;
}
